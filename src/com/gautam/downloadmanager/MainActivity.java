package com.gautam.downloadmanager;

import com.sourcebits.gautam.service.BoundServiceImpl;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends Activity {
	private MyService mBoundService;
	final ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "disconnected");
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			MyService binderImpl = (MyService) service;
			mBoundService = binderImpl.getService();
			mBoundService.executeService();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
