package com.gautam.downloadmanager;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

public class MyService extends Activity {
	/** Called when the activity is first created. */
	private BoundServiceImpl mBoundService;
	private static final String TAG = ServiceActivity.class.getCanonicalName();
	final ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "disconnected");
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			IBinderImpl binderImpl = (IBinderImpl) service;
			mBoundService = binderImpl.getService();
			mBoundService.executeService();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
	}

	public void startService(View view) {
		Runnable serviceRunnable = new Runnable() {

			public void run() {
				
				startService();
			}
		};
		new Thread(serviceRunnable).start();
	}

	public void unBindService(View view) {
		unbindService(serviceConnection);
	}

	public void bindService(View view) {
		Intent bindIntent = new Intent(ServiceActivity.this,
				BoundServiceImpl.class);
		bindService(bindIntent, serviceConnection, BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		mBoundService.unbindService(serviceConnection);
		super.onStop();
	}

}
